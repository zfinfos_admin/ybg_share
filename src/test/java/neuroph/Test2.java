package neuroph;

import com.ybg.share.ShareApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.neuroph.core.NeuralNetwork;
import org.neuroph.core.data.DataSet;
import org.neuroph.core.data.DataSetRow;
import org.neuroph.nnet.Perceptron;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest(classes = ShareApplication.class)
@RunWith(SpringRunner.class)
public class Test2 {

    @Test
    public void test(){
        // create new perceptron network
        NeuralNetwork neuralNetwork = new Perceptron(2, 1);
// create training set
        DataSet trainingSet = new DataSet(2, 1);
// add training data to training set (logical OR function)
        trainingSet. add (new DataSetRow(new double[]{0, 0}, new double[]{0}));
        trainingSet. add (new DataSetRow (new double[]{0, 1}, new double[]{1}));
        trainingSet. add (new DataSetRow (new double[]{1, 0}, new double[]{1}));
        trainingSet. add (new DataSetRow (new double[]{1, 1}, new double[]{1}));
// learn the training set
        neuralNetwork.learn(trainingSet);
         neuralNetwork.setInput(1, 1);
        neuralNetwork.calculate();
        double[] networkOutput = neuralNetwork.getOutput();
        System.out.println(networkOutput[0]);
    }
}
