import com.ybg.share.ShareApplication;
import com.ybg.share.core.serviceapi.LoadDataServiceApi;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest(classes = ShareApplication.class)
@RunWith(SpringRunner.class)
public class TestLoadDate {

    @Autowired
    LoadDataServiceApi loadDateServiceApi;

    @Test
    public void test() {

        loadDateServiceApi.loadData();
        System.out.println("执行完毕");
    }

}
