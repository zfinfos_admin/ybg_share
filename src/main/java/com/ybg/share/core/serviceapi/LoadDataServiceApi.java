package com.ybg.share.core.serviceapi;

import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ybg.share.core.dbapi.entity.ShareExeRecord;
import com.ybg.share.core.dbapi.entity.ShareLastDayRecord;
import com.ybg.share.core.dbapi.entity.ShareStock;
import com.ybg.share.core.dbapi.entity.ShareStockDay;
import com.ybg.share.core.dbapi.service.ShareExeRecordService;
import com.ybg.share.core.dbapi.service.ShareLastDayRecordService;
import com.ybg.share.core.dbapi.service.ShareStockDayService;
import com.ybg.share.core.dbapi.service.ShareStockService;
import com.ybg.share.core.shareapi.bo.ShareStockDayBO;
import com.ybg.share.core.shareapi.dto.QueryDayDTO;
import com.ybg.share.core.shareapi.impl.YiyuanMarketApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class LoadDataServiceApi {

    @Autowired
    YiyuanMarketApi yiyuanMarketApi;
    @Autowired
    ShareExeRecordService shareExeRecordService;
    @Autowired
    ShareStockDayService shareStockDayService;
    @Autowired
    ShareStockService shareStockService;
    @Autowired
    ShareLastDayRecordService shareLastDayRecordService;

    public Boolean loadData() {
        QueryWrapper<ShareStock> wrapper = new QueryWrapper<>();
        wrapper.eq(ShareStock.STATUS, true);
        List<ShareStock> list = shareStockService.list(wrapper);
        for (ShareStock data : list) {
            loadData(data.getCode());
        }
        return true;
    }

    private void loadData(String code) {
        int monthoffset = 30;//拉取三年的日K数据

        Date date = new Date();
        saveData(code, date);
        date = DateUtil.beginOfMonth(new Date());
        for (int i = 1; i < monthoffset; i++) {
            date = DateUtil.offset(date, DateField.MONTH, -1);
            boolean hadrun = shareExeRecordService.hadRun(code, DateUtil.format(date, "yyyy-MM"));
            if (!hadrun) {
                ShareExeRecord record = new ShareExeRecord();
                record.setMonth(DateUtil.format(date, "yyyy-MM"));
                record.setCode(code);
                shareExeRecordService.save(record);
                saveData(code, date);
            }

        }


    }


    private void saveData(String code, Date date) {
        boolean hadRun = shareLastDayRecordService.hadRun(date, code);
        if (hadRun) {
            //已经执行过了
            return;
        }
        ShareLastDayRecord hadRunRecord = new ShareLastDayRecord();
        hadRunRecord.setCode(code);
        hadRunRecord.setLastDate(DateUtil.formatDate(date));
        shareLastDayRecordService.save(hadRunRecord);
        QueryDayDTO dto = new QueryDayDTO();
        dto.setCode(code);
        dto.setBegin(DateUtil.formatDate(DateUtil.beginOfMonth(date)));
        dto.setEnd(DateUtil.formatDate(DateUtil.endOfMonth(date)));
        List<ShareStockDayBO> dayDate = yiyuanMarketApi.getDayDate(dto);
        if (dayDate == null) {
            return;
        }
        for (ShareStockDayBO shareStockDayBO : dayDate) {
            ShareStockDay bo = new ShareStockDay();
            bo.setClosePrice(shareStockDayBO.getClosePrice());
            bo.setCode(shareStockDayBO.getCode());
            bo.setDate(shareStockDayBO.getDate());
            bo.setMarket(shareStockDayBO.getMarket());
            bo.setMaxPrice(shareStockDayBO.getMaxPrice());
            bo.setMinPrice(shareStockDayBO.getMinPrice());
            bo.setOpenPrice(shareStockDayBO.getOpenPrice());
            bo.setStockName(shareStockDayBO.getStockName());
            bo.setTradeMoney(shareStockDayBO.getTradeMoney());
            bo.setTradeNum(shareStockDayBO.getTradeNum());
            ShareStockDay dayRecord = shareStockDayService.getDayRecord(code, shareStockDayBO.getDate());
            if (dayRecord == null) {
                shareStockDayService.save(bo);
            } else {
                bo.setId(dayRecord.getId());
                shareStockDayService.updateById(bo);
            }


        }
    }

}
