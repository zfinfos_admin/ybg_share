package com.ybg.share.core.shareapi.impl;


import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import com.ybg.share.core.shareapi.AbstractStockIndexApi;
import com.ybg.share.framework.enums.MarketEnum;

import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * 拉取上海交易所大盘数据
 */
@Service
public class ShanghaiStockIndexApi extends AbstractStockIndexApi {


    @Override
    public String getUrl() {
        String today= DateUtil.format(new Date(), DatePattern.PURE_DATE_PATTERN);
        return "http://quotes.money.163.com/service/chddata.html?code=0000001&" +
                "start="+MarketEnum.sh.getStartTime()+"&end=" + today  +
                "&fields=TCLOSE;HIGH;LOW;TOPEN;LCLOSE;CHG;PCHG;VOTURNOVER;VATURNOVER";
    }

    @Override
    public String getMarket() {
        return MarketEnum.sh.getCode();
    }


}
