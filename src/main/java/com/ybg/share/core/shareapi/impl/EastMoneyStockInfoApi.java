package com.ybg.share.core.shareapi.impl;

import com.ybg.share.core.shareapi.AbstractStockInfoApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;

/**
 * 获取市场交易所所有股票 列表
 */
@Service
public class EastMoneyStockInfoApi extends AbstractStockInfoApi {
    private static final String STOCK_LIST = "http://quote.eastmoney.com/stock_list.html";
    @Autowired
    PageProcessor pageProcessor;

    @Override
    public void execute() {
        Spider.create(pageProcessor)
                .addUrl(STOCK_LIST)
                .thread(100).run();
    }
}
