package com.ybg.share.core.shareapi.impl;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "yiyuan")
public class YiyuanConfig {
    String appId;
    String secret;
    String code;

}
