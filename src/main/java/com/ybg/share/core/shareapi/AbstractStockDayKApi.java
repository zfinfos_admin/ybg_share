package com.ybg.share.core.shareapi;

import com.ybg.share.core.dbapi.entity.ShareStockDayK;
import com.ybg.share.core.dbapi.service.ShareStockDayKService;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractStockDayKApi {
    @Autowired
    ShareStockDayKService shareStockDayKService;
    /**
     * 执行
     *
     * @return 是否执行
     */
    public Boolean execute() {
        String url = getUrl();
        String market = getMarket();
        saveToDB(url, market);
        return Boolean.TRUE;
    }

    public abstract String getUrl();

    public abstract String getMarket();

    private void saveToDB(String url, String market){
        List<ShareStockDayK> shareStockDayKList = new ArrayList<>();
        CloseableHttpClient client = null;
        CloseableHttpResponse response = null;
        try {
            HttpGet httpGet = new HttpGet(url);
            client = HttpClients.createDefault();
            response = client.execute(httpGet);
            HttpEntity entity = response.getEntity();
            BufferedReader in = new BufferedReader(new InputStreamReader(entity.getContent(), "GBK"));
            String result;
            int index = 0;
            while ((result = in.readLine()) != null) {
                if (index != 0) {
                    String[] dataOfThisLine = result.split(",");
                    ShareStockDayK shareStockDayK = new ShareStockDayK();
                    shareStockDayK.setDate(checkIsNone(dataOfThisLine[0]));
                    shareStockDayK.setCode(checkIsNone(dataOfThisLine[1].substring(1)));
                    shareStockDayK.setName(checkIsNone(dataOfThisLine[2]));
                    shareStockDayK.setClosePrice(new BigDecimal(checkIsNone(dataOfThisLine[3])));
                    shareStockDayK.setMaxPrice(new BigDecimal(checkIsNone(dataOfThisLine[4])));
                    shareStockDayK.setMinPrice(new BigDecimal(checkIsNone(dataOfThisLine[5])));
                    shareStockDayK.setOpenPrice(new BigDecimal(checkIsNone(dataOfThisLine[6])));
                    shareStockDayK.setBeforeClose(new BigDecimal(checkIsNone(dataOfThisLine[7])));
                    shareStockDayK.setChangeAmount(new BigDecimal(checkIsNone(dataOfThisLine[8])));
                    shareStockDayK.setChangeRange(new BigDecimal(checkIsNone(dataOfThisLine[9])));
                    shareStockDayK.setTurnoverRate(new BigDecimal(checkIsNone(dataOfThisLine[10])));
                    shareStockDayK.setTradeNum(new BigDecimal(checkIsNone(dataOfThisLine[11])).longValue());
                    shareStockDayK.setTradeMoney(new BigDecimal(checkIsNone(dataOfThisLine[12])));
                    shareStockDayK.setTotalValue(new BigDecimal(checkIsNone(dataOfThisLine[13])).longValue());
                    shareStockDayK.setCirculationValue(new BigDecimal(checkIsNone(dataOfThisLine[14])).longValue());
                    shareStockDayK.setTurnoverNum(0L);//倒出来的数据为None
                    shareStockDayK.setMarket(getMarket());
                    shareStockDayKList.add(shareStockDayK);
                }
                index++;
            }
            for (ShareStockDayK shareStockDayK:shareStockDayKList){
                shareStockDayKService.saveIgnore(shareStockDayK);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private String checkIsNone(String var) {
        if (var.equals("None")) {
            return "0";
        }
        return var;
    }
}
