package com.ybg.share.core.shareapi.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class QueryDayDTO implements Serializable {
    /**
     * 开始日期，格式yyyy-MM-dd，最早的时间为2000-01-01日。属于未复权数据。
     */
    String begin;
    /**
     * 股票编码，不需要写市场名
     */
    String code;
    /**
     * 结束日期，格式yyyy-MM-dd，注意时间范围为31天
     */
    String end;
}
