package com.ybg.share.core.shareapi.bo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;


/**
 * <p>
 * 日K数据
 * </p>
 *
 * @author yanyu
 * @since 2019-10-13
 */
@ApiModel(value = "日K数据")
public class ShareStockDayBO extends Model<ShareStockDayBO> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */

    @ApiModelProperty(value = "主键")
    private Long id;
    /**
     * 昨日收盘价
     */
    @ApiModelProperty(value = "昨日收盘价")
    private BigDecimal closePrice;
    /**
     * 股票代码
     */
    @ApiModelProperty(value = "股票代码")
    private String code;
    /**
     * 日期，例如2015-09-02
     */
    @ApiModelProperty(value = "日期，例如2015-09-02")
    private String date;
    /**
     * 市场，例如sh, sz
     */
    @ApiModelProperty(value = "市场，例如sh, sz")
    private String market;
    /**
     * 最高价
     */
    @ApiModelProperty(value = "最高价")
    private BigDecimal maxPrice;
    /**
     * 最低价
     */
    @ApiModelProperty(value = "最低价")
    private BigDecimal minPrice;
    /**
     * 开盘价
     */
    @ApiModelProperty(value = "开盘价")
    private BigDecimal openPrice;
    /**
     * 股票名称
     */
    @ApiModelProperty(value = "股票名称")
    private String stockName;
    /**
     * 交易金额
     */
    @ApiModelProperty(value = "交易金额")
    private BigDecimal tradeMoney;
    /**
     * 交易数量
     */
    @ApiModelProperty(value = "交易数量")
    private Long tradeNum;


    /**
     * 获取主键
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置主键
     */

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取昨日收盘价
     */
    public BigDecimal getClosePrice() {
        return closePrice;
    }

    /**
     * 设置昨日收盘价
     */

    public void setClosePrice(BigDecimal closePrice) {
        this.closePrice = closePrice;
    }

    /**
     * 获取股票代码
     */
    public String getCode() {
        return code;
    }

    /**
     * 设置股票代码
     */

    public void setCode(String code) {
        this.code = code;
    }

    /**
     * 获取日期，例如2015-09-02
     */
    public String getDate() {
        return date;
    }

    /**
     * 设置日期，例如2015-09-02
     */

    public void setDate(String date) {
        this.date = date;
    }

    /**
     * 获取市场，例如sh, sz
     */
    public String getMarket() {
        return market;
    }

    /**
     * 设置市场，例如sh, sz
     */

    public void setMarket(String market) {
        this.market = market;
    }

    /**
     * 获取最高价
     */
    public BigDecimal getMaxPrice() {
        return maxPrice;
    }

    /**
     * 设置最高价
     */

    public void setMaxPrice(BigDecimal maxPrice) {
        this.maxPrice = maxPrice;
    }

    /**
     * 获取最低价
     */
    public BigDecimal getMinPrice() {
        return minPrice;
    }

    /**
     * 设置最低价
     */

    public void setMinPrice(BigDecimal minPrice) {
        this.minPrice = minPrice;
    }

    /**
     * 获取开盘价
     */
    public BigDecimal getOpenPrice() {
        return openPrice;
    }

    /**
     * 设置开盘价
     */

    public void setOpenPrice(BigDecimal openPrice) {
        this.openPrice = openPrice;
    }

    /**
     * 获取股票名称
     */
    public String getStockName() {
        return stockName;
    }

    /**
     * 设置股票名称
     */

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    /**
     * 获取交易金额
     */
    public BigDecimal getTradeMoney() {
        return tradeMoney;
    }

    /**
     * 设置交易金额
     */

    public void setTradeMoney(BigDecimal tradeMoney) {
        this.tradeMoney = tradeMoney;
    }

    /**
     * 获取交易数量
     */
    public Long getTradeNum() {
        return tradeNum;
    }

    /**
     * 设置交易数量
     */

    public void setTradeNum(Long tradeNum) {
        this.tradeNum = tradeNum;
    }

    /**
     * 主键列的数据库字段名称
     */
    public static final String ID = "id";

    /**
     * 昨日收盘价列的数据库字段名称
     */
    public static final String CLOSE_PRICE = "close_price";

    /**
     * 股票代码列的数据库字段名称
     */
    public static final String CODE = "code";

    /**
     * 日期，例如2015-09-02列的数据库字段名称
     */
    public static final String DATE = "date";

    /**
     * 市场，例如sh, sz列的数据库字段名称
     */
    public static final String MARKET = "market";

    /**
     * 最高价列的数据库字段名称
     */
    public static final String MAX_PRICE = "max_price";

    /**
     * 最低价列的数据库字段名称
     */
    public static final String MIN_PRICE = "min_price";

    /**
     * 开盘价列的数据库字段名称
     */
    public static final String OPEN_PRICE = "open_price";

    /**
     * 股票名称列的数据库字段名称
     */
    public static final String STOCK_NAME = "stock_name";

    /**
     * 交易金额列的数据库字段名称
     */
    public static final String TRADE_MONEY = "trade_money";

    /**
     * 交易数量列的数据库字段名称
     */
    public static final String TRADE_NUM = "trade_num";

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "ShareStockDay{" +
                "id=" + id +
                ", closePrice=" + closePrice +
                ", code=" + code +
                ", date=" + date +
                ", market=" + market +
                ", maxPrice=" + maxPrice +
                ", minPrice=" + minPrice +
                ", openPrice=" + openPrice +
                ", stockName=" + stockName +
                ", tradeMoney=" + tradeMoney +
                ", tradeNum=" + tradeNum +
                "}";
    }
}
