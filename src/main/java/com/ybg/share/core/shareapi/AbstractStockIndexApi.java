package com.ybg.share.core.shareapi;

import com.ybg.share.core.dbapi.entity.ShareStockIndex;
import com.ybg.share.core.dbapi.service.ShareCompositeIndexService;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 获取大盘日K交易信息
 */
public abstract class AbstractStockIndexApi {
    @Autowired
    ShareCompositeIndexService shareCompositeIndexService;

    /**
     * 执行
     *
     * @return 是否执行
     */
    public Boolean execute() {
        String url = getUrl();
        String market = getMarket();
        saveToDB(url, market);
        return Boolean.TRUE;
    }

    /**
     * 获取拉取地址
     *
     * @return 地址
     */
    public abstract String getUrl();

    /**
     * 市场简称
     *
     * @return 市场
     */
    public abstract String getMarket();

    /**
     * 保存到数据库
     *
     * @param url 地址
     * @param market 市场
     */
    private void saveToDB(String url, String market) {
        List<ShareStockIndex> list = new ArrayList<>();
        try {
            CloseableHttpClient client = null;
            CloseableHttpResponse response = null;
            try {
                HttpGet httpGet = new HttpGet(url);
                client = HttpClients.createDefault();
                response = client.execute(httpGet);
                HttpEntity entity = response.getEntity();
                BufferedReader in = new BufferedReader(new InputStreamReader(entity.getContent(), "GBK"));
                String result;
                int index = 0;
                while ((result = in.readLine()) != null) {
                    System.out.println(result);
                    //剔除第一行的数据，其他插入到数据库
                    if (index != 0) {
                        ShareStockIndex shareStockIndex = new ShareStockIndex();
                        String[] dataOfThisLine = result.split(",");
                        shareStockIndex.setDate(checkIsNone(dataOfThisLine[0]));
                        shareStockIndex.setCode(checkIsNone(dataOfThisLine[1].substring(1)));
                        shareStockIndex.setStockName(checkIsNone(dataOfThisLine[2]));
                        shareStockIndex.setClosePrice(new BigDecimal(checkIsNone(dataOfThisLine[3])));
                        shareStockIndex.setMaxPrice(new BigDecimal(checkIsNone(dataOfThisLine[4])));
                        shareStockIndex.setMinPrice(new BigDecimal(checkIsNone(dataOfThisLine[5])));
                        shareStockIndex.setOpenPrice(new BigDecimal(checkIsNone(dataOfThisLine[6])));
                        shareStockIndex.setBeforeClose(new BigDecimal(checkIsNone(dataOfThisLine[7])));
                        shareStockIndex.setChangeAmount(new BigDecimal(checkIsNone(dataOfThisLine[8])));
                        shareStockIndex.setChangeRange(new BigDecimal(checkIsNone(dataOfThisLine[9])));
                        shareStockIndex.setTradeNum(new BigDecimal(checkIsNone(dataOfThisLine[10])).longValue());
                        shareStockIndex.setTradeMoney(new BigDecimal(checkIsNone(dataOfThisLine[11])));
                        shareStockIndex.setMarket(market);
                        list.add(shareStockIndex);
                    }
                    index++;
                }
                for (ShareStockIndex shareStockIndex : list) {
                    shareCompositeIndexService.insertIgnore(shareStockIndex);
                }
            } finally {
                if (response != null) {
                    response.close();
                }
                if (client != null) {
                    client.close();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private String checkIsNone(String var) {
        if (var.equals("None")) {
            return "0";
        }
        return var;
    }
}
