package com.ybg.share.core.shareapi.impl;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import com.ybg.share.core.shareapi.AbstractStockDayKApi;
import com.ybg.share.framework.enums.MarketEnum;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class ShenzhenStockDayKApi extends AbstractStockDayKApi {
    @Override
    public String getUrl() {
        String today= DateUtil.format(new Date(), DatePattern.PURE_DATE_PATTERN);
        return "http://quotes.money.163.com/service/chddata.html?code=1000725&" +
                "start="+ MarketEnum.sh.getStartTime() +"&end=" + today +
                "&fields=TCLOSE;HIGH;LOW;TOPEN;LCLOSE;CHG;PCHG;TURNOVER;VOTURNOVER;VATURNOVER;TCAP;MCAP";
    }

    @Override
    public String getMarket() {
        return MarketEnum.sz.getCode();
    }
}
