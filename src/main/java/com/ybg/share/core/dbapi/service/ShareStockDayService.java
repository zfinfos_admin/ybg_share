package com.ybg.share.core.dbapi.service;

import com.ybg.share.core.dbapi.entity.ShareStockDay;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 日K基本数据 服务类
 * </p>
 *
 * @author yanyu
 * @since 2019-10-13
 */
public interface ShareStockDayService extends IService<ShareStockDay> {
    ShareStockDay getDayRecord(String code, String date);
}
