package com.ybg.share.core.dbapi.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.baomidou.mybatisplus.annotation.IdType;

import com.baomidou.mybatisplus.extension.activerecord.Model;

import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;


/**
 * <p>
 * 股票信息
 * </p>
 *
 * @author yanyu
 * @since 2019-10-13
 */
@ApiModel(value = "股票信息")
public class ShareStock extends Model<ShareStock> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(value = "主键")
    private Long id;
    /**
     * 股票名称
     */
    @ApiModelProperty(value = "股票名称")
    private String name;
    /**
     * 1 可用，0禁用
     */
    @ApiModelProperty(value = "1 可用，0禁用")
    private String code;
    /**
     * 所属市场
     */
    @ApiModelProperty(value = "所属市场")
    private String market;
    /**
     * 1 可用 0不可用
     */
    @ApiModelProperty(value = "1 可用 0不可用")
    private Boolean status;


    /**
     * 获取主键
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置主键
     */

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取股票名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置股票名称
     */

    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取1 可用，0禁用
     */
    public String getCode() {
        return code;
    }

    /**
     * 设置1 可用，0禁用
     */

    public void setCode(String code) {
        this.code = code;
    }

    /**
     * 获取所属市场
     */
    public String getMarket() {
        return market;
    }

    /**
     * 设置所属市场
     */

    public void setMarket(String market) {
        this.market = market;
    }

    /**
     * 获取1 可用 0不可用
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     * 设置1 可用 0不可用
     */

    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     * 主键列的数据库字段名称
     */
    public static final String ID = "id";

    /**
     * 股票名称列的数据库字段名称
     */
    public static final String NAME = "name";

    /**
     * 1 可用，0禁用列的数据库字段名称
     */
    public static final String CODE = "code";

    /**
     * 所属市场列的数据库字段名称
     */
    public static final String MARKET = "market";

    /**
     * 1 可用 0不可用列的数据库字段名称
     */
    public static final String STATUS = "status";

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "ShareStock{" +
                "id=" + id +
                ", name=" + name +
                ", code=" + code +
                ", market=" + market +
                ", status=" + status +
                "}";
    }
}
