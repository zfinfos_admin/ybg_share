package com.ybg.share.core.dbapi.dao;

import com.ybg.share.core.dbapi.entity.ShareStockDayK;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Insert;

/**
 * <p>
 * 行情日k数据 Mapper 接口
 * </p>
 *
 * @author Maokun.zhong
 * @since 2019-10-21
 */
public interface ShareStockDayKMapper extends BaseMapper<ShareStockDayK> {

    @Insert("insert ignore into share_stock_day_k " +
            "(`date`,`code`,`market`,`name`,`close_price`,`max_price`,`min_price`,`open_price`,`before_close`,`change_amount`," +
            "`change_range`,`turnover_rate`,`trade_num`,`trade_money`,`total_value`,`circulation_value`,`turnover_num`) values " +
            "(#{date},#{code},#{market},#{name},#{closePrice},#{maxPrice},#{minPrice},#{openPrice},#{beforeClose},#{changeAmount}," +
            "#{changeRange},#{turnoverRate},#{tradeNum},#{tradeMoney},#{totalValue},#{circulationValue},#{turnoverNum})")
    boolean saveIgnore(ShareStockDayK shareStockDayK);

}
