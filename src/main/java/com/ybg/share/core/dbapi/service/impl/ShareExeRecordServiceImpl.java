package com.ybg.share.core.dbapi.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ybg.share.core.dbapi.entity.ShareExeRecord;
import com.ybg.share.core.dbapi.dao.ShareExeRecordMapper;
import com.ybg.share.core.dbapi.service.ShareExeRecordService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
//import com.alibaba.dubbo.config.annotation.Service;

/**
 * <p>
 * 执行记录 服务实现类
 * </p>
 *
 * @author yanyu
 * @since 2019-10-13
 */
//@Service(version = "1.0.0", interfaceClass = ShareExeRecordService.class)
@Service
public class ShareExeRecordServiceImpl extends ServiceImpl<ShareExeRecordMapper, ShareExeRecord> implements ShareExeRecordService {

    @Override
    public Boolean hadRun(String code, String month) {
        QueryWrapper<ShareExeRecord> wrapper = new QueryWrapper<>();
        wrapper.eq(ShareExeRecord.CODE, code);
        wrapper.eq(ShareExeRecord.MONTH, month);
        ShareExeRecord bean = baseMapper.selectOne(wrapper);
        return bean != null;
    }
}
