package com.ybg.share.core.dbapi.service.impl;

import com.ybg.share.core.dbapi.entity.ShareStockDayK;
import com.ybg.share.core.dbapi.dao.ShareStockDayKMapper;
import com.ybg.share.core.dbapi.service.ShareStockDayKService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
//import com.alibaba.dubbo.config.annotation.Service;
/**
 * <p>
 * 行情日k数据 服务实现类
 * </p>
 *
 * @author Maokun.zhong
 * @since 2019-10-21
 */
//@Service(version = "1.0.0", interfaceClass = ShareStockDayKService.class)
@Service
public class ShareStockDayKServiceImpl extends ServiceImpl<ShareStockDayKMapper, ShareStockDayK> implements ShareStockDayKService {
    @Autowired
    ShareStockDayKMapper shareStockDayKMapper;

    @Override
    public boolean saveIgnore(ShareStockDayK shareStockDayK) {
        return shareStockDayKMapper.saveIgnore(shareStockDayK);
    }
}
