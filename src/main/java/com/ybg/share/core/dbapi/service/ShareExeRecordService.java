package com.ybg.share.core.dbapi.service;

import com.ybg.share.core.dbapi.entity.ShareExeRecord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 执行记录 服务类
 * </p>
 *
 * @author yanyu
 * @since 2019-10-13
 */
public interface ShareExeRecordService extends IService<ShareExeRecord> {


    Boolean hadRun(String code, String month);

}
