package com.ybg.share.core.dbapi.dao;

import com.ybg.share.core.dbapi.entity.ShareStock;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Insert;

/**
 * <p>
 * 股票信息 Mapper 接口
 * </p>
 *
 * @author yanyu
 * @since 2019-10-13
 */
public interface ShareStockMapper extends BaseMapper<ShareStock> {

    @Insert("insert ignore into share_stock (`name`,`code`,`market`) values(#{name},#{code},#{market})")
    boolean saveIgnore(ShareStock shareStock);

    @Insert("insert replace into share_stock (`name`,`code`,`market`) values(#{name},#{code},#{market})")
    boolean saveReplace(ShareStock shareStock);
}
