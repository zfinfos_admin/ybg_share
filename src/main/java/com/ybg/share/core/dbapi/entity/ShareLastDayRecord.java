package com.ybg.share.core.dbapi.entity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.baomidou.mybatisplus.annotation.TableName;

import com.baomidou.mybatisplus.annotation.IdType;

import com.baomidou.mybatisplus.extension.activerecord.Model;

import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;


/**
 * <p>
 * 股票最后一次加载日期执行时间
 * </p>
 *
 * @author yanyu
 * @since 2019-10-13
 */
@ApiModel(value="股票最后一次加载日期执行时间") 
@TableName("share_last_day_record")
public class ShareLastDayRecord extends Model<ShareLastDayRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
@ApiModelProperty(value = "主键")
    private Long id;
    /**
     * 股票代码
     */
@ApiModelProperty(value = "股票代码")
    private String code;
    /**
     * 最后一次执行事件
     */
@ApiModelProperty(value = "最后一次执行事件")
    private String lastDate;


    /**
     * 获取主键
     */
    public Long getId() {
        return id;
    }
    /**
     * 设置主键
     */

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取股票代码
     */
    public String getCode() {
        return code;
    }
    /**
     * 设置股票代码
     */

    public void setCode(String code) {
        this.code = code;
    }

    /**
     * 获取最后一次执行事件
     */
    public String getLastDate() {
        return lastDate;
    }
    /**
     * 设置最后一次执行事件
     */

    public void setLastDate(String lastDate) {
        this.lastDate = lastDate;
    }

    /**
     * 主键列的数据库字段名称
     */
    public static final String ID = "id";

    /**
     * 股票代码列的数据库字段名称
     */
    public static final String CODE = "code";

    /**
     * 最后一次执行事件列的数据库字段名称
     */
    public static final String LAST_DATE = "last_date";

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "ShareLastDayRecord{" +
        "id=" + id +
        ", code=" + code +
        ", lastDate=" + lastDate +
        "}";
    }
}
