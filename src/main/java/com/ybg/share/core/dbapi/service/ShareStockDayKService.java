package com.ybg.share.core.dbapi.service;

import com.ybg.share.core.dbapi.entity.ShareStockDayK;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 行情日k数据 服务类
 * </p>
 *
 * @author Maokun.zhong
 * @since 2019-10-21
 */
public interface ShareStockDayKService extends IService<ShareStockDayK> {

    boolean saveIgnore(ShareStockDayK shareStockDayK);
}
