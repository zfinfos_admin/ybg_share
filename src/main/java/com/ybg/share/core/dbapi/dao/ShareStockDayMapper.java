package com.ybg.share.core.dbapi.dao;

import com.ybg.share.core.dbapi.entity.ShareStockDay;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 日K基本数据 Mapper 接口
 * </p>
 *
 * @author yanyu
 * @since 2019-10-13
 */
public interface ShareStockDayMapper extends BaseMapper<ShareStockDay> {

}
