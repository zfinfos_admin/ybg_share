package com.ybg.share.core.dbapi.dao;

import com.ybg.share.core.dbapi.entity.ShareLastDayRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 股票最后一次加载日期执行时间 Mapper 接口
 * </p>
 *
 * @author yanyu
 * @since 2019-10-13
 */
public interface ShareLastDayRecordMapper extends BaseMapper<ShareLastDayRecord> {

}
