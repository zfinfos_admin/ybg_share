package com.ybg.share.core.dbapi.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ybg.share.core.dbapi.entity.ShareStockDay;
import com.ybg.share.core.dbapi.dao.ShareStockDayMapper;
import com.ybg.share.core.dbapi.service.ShareStockDayService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
//import com.alibaba.dubbo.config.annotation.Service;

/**
 * <p>
 * 日K基本数据 服务实现类
 * </p>
 *
 * @author yanyu
 * @since 2019-10-13
 */
//@Service(version = "1.0.0", interfaceClass = ShareStockDayService.class)
@Service
public class ShareStockDayServiceImpl extends ServiceImpl<ShareStockDayMapper, ShareStockDay> implements ShareStockDayService {

    @Override
    public ShareStockDay getDayRecord(String code, String date) {
        QueryWrapper<ShareStockDay> wrapper = new QueryWrapper<>();
        wrapper.eq(ShareStockDay.CODE, code);
        wrapper.eq(ShareStockDay.DATE, date);
        return baseMapper.selectOne(wrapper);
    }
}
