package com.ybg.share.core.dbapi.service;

import com.ybg.share.core.dbapi.entity.ShareStock;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 股票信息 服务类
 * </p>
 *
 * @author yanyu
 * @since 2019-10-13
 */
public interface ShareStockService extends IService<ShareStock> {

    boolean saveIgnore(ShareStock shareStock);

    boolean saveReplace(ShareStock shareStock);
}
