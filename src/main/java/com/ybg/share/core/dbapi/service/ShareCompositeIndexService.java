package com.ybg.share.core.dbapi.service;

import com.ybg.share.core.dbapi.entity.ShareStockIndex;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 上证指数和深证指数 服务类
 * </p>
 *
 * @author Maokun.zhong
 * @since 2019-10-17
 */
public interface ShareCompositeIndexService extends IService<ShareStockIndex> {

    boolean insertIgnore(ShareStockIndex shareStockIndex);
}
