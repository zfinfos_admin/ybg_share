package com.ybg.share.core.dbapi.dao;

import com.ybg.share.core.dbapi.entity.ShareExeRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 执行记录 Mapper 接口
 * </p>
 *
 * @author yanyu
 * @since 2019-10-13
 */
public interface ShareExeRecordMapper extends BaseMapper<ShareExeRecord> {

}
