package com.ybg.share.core.dbapi.service.impl;

import com.ybg.share.core.dbapi.entity.ShareStockIndex;
import com.ybg.share.core.dbapi.dao.ShareStockIndexMapper;
import com.ybg.share.core.dbapi.service.ShareCompositeIndexService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
//import com.alibaba.dubbo.config.annotation.Service;
/**
 * <p>
 * 上证指数和深证指数 服务实现类
 * </p>
 *
 * @author Maokun.zhong
 * @since 2019-10-17
 */
//@Service(version = "1.0.0", interfaceClass = ShareCompositeIndexService.class)
@Service
public class ShareStockIndexServiceImpl extends ServiceImpl<ShareStockIndexMapper, ShareStockIndex> implements ShareCompositeIndexService {


    @Override
    public boolean insertIgnore(ShareStockIndex shareStockIndex) {
        return baseMapper.insertIgnore(shareStockIndex);
    }
}
