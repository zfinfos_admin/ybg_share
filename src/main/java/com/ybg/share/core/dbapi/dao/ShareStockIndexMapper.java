package com.ybg.share.core.dbapi.dao;

import com.ybg.share.core.dbapi.entity.ShareStockIndex;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Insert;

/**
 * <p>
 * 上证指数和深证指数 Mapper 接口
 * </p>
 *
 * @author Maokun.zhong
 * @since 2019-10-17
 */
public interface ShareStockIndexMapper extends BaseMapper<ShareStockIndex> {

    @Insert("insert ignore into share_composite_index " +
            "(`date`,`code`,`market`,`stock_name`,`close_price`,`max_price`,`min_price`,`open_price`," +
            "`before_close`,`change_amount`,`change_range`,`trade_num`,`trade_money`) values" +
            "(#{date},#{code},#{market},#{stockName},#{closePrice},#{maxPrice},#{minPrice},#{openPrice}," +
            "#{beforeClose},#{changeAmount},#{changeRange},#{tradeNum},#{tradeMoney})")
    boolean insertIgnore(ShareStockIndex shareStockIndex);
}
