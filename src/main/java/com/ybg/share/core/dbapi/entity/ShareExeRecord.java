package com.ybg.share.core.dbapi.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.baomidou.mybatisplus.annotation.IdType;

import com.baomidou.mybatisplus.extension.activerecord.Model;

import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;


/**
 * <p>
 * 执行记录
 * </p>
 *
 * @author yanyu
 * @since 2019-10-13
 */
@ApiModel(value = "执行记录")
public class ShareExeRecord extends Model<ShareExeRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(value = "主键")
    private Long id;
    /**
     * 已经执行的月份 如1991-10
     */
    @ApiModelProperty(value = "已经执行的月份 如1991-10")
    private String month;
    /**
     * 股票代码
     */
    @ApiModelProperty(value = "股票代码")
    private String code;


    /**
     * 获取主键
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置主键
     */

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取已经执行的月份 如1991-10
     */
    public String getMonth() {
        return month;
    }

    /**
     * 设置已经执行的月份 如1991-10
     */

    public void setMonth(String month) {
        this.month = month;
    }

    /**
     * 获取股票代码
     */
    public String getCode() {
        return code;
    }

    /**
     * 设置股票代码
     */

    public void setCode(String code) {
        this.code = code;
    }

    /**
     * 主键列的数据库字段名称
     */
    public static final String ID = "id";

    /**
     * 已经执行的月份 如1991-10列的数据库字段名称
     */
    public static final String MONTH = "month";

    /**
     * 股票代码列的数据库字段名称
     */
    public static final String CODE = "code";

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "ShareExeRecord{" +
                "id=" + id +
                ", month=" + month +
                ", code=" + code +
                "}";
    }
}
