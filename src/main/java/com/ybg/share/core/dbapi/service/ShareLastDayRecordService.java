package com.ybg.share.core.dbapi.service;

import com.ybg.share.core.dbapi.entity.ShareLastDayRecord;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Date;

/**
 * <p>
 * 股票最后一次加载日期执行时间 服务类
 * </p>
 *
 * @author yanyu
 * @since 2019-10-13
 */
public interface ShareLastDayRecordService extends IService<ShareLastDayRecord> {

    /**
     * 检测当天有没有执行
     * @param date
     * @return
     */
    boolean hadRun(Date date,String code);
}
