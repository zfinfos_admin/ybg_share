package com.ybg.share.core.dbapi.entity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.IdType;

import com.baomidou.mybatisplus.extension.activerecord.Model;

import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;


/**
 * <p>
 * 行情日k数据
 * </p>
 *
 * @author Maokun.zhong
 * @since 2019-10-21
 */
@ApiModel(value="行情日k数据") 
public class ShareStockDayK extends Model<ShareStockDayK> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
@ApiModelProperty(value = "主键")
    private Long id;
    /**
     * 日期，2019-10-16
     */
@ApiModelProperty(value = "日期，2019-10-16")
    private String date;
    /**
     * 股票代码
     */
@ApiModelProperty(value = "股票代码")
    private String code;
    /**
     * 股票名称
     */
@ApiModelProperty(value = "股票名称")
    private String name;
    /**
     * 所属市场
     */
@ApiModelProperty(value = "所属市场")
    private String market;
    /**
     * 收盘价
     */
@ApiModelProperty(value = "收盘价")
    private BigDecimal closePrice;
    /**
     * 最高价
     */
@ApiModelProperty(value = "最高价")
    private BigDecimal maxPrice;
    /**
     * 最低价
     */
@ApiModelProperty(value = "最低价")
    private BigDecimal minPrice;
    /**
     * 开盘价
     */
@ApiModelProperty(value = "开盘价")
    private BigDecimal openPrice;
    /**
     * 前收盘
     */
@ApiModelProperty(value = "前收盘")
    private BigDecimal beforeClose;
    /**
     * 涨跌额
     */
@ApiModelProperty(value = "涨跌额")
    private BigDecimal changeAmount;
    /**
     * 涨跌幅
     */
@ApiModelProperty(value = "涨跌幅")
    private BigDecimal changeRange;
    /**
     * 换手率
     */
@ApiModelProperty(value = "换手率")
    private BigDecimal turnoverRate;
    /**
     * 成交量
     */
@ApiModelProperty(value = "成交量")
    private Long tradeNum;
    /**
     * 成交金额
     */
@ApiModelProperty(value = "成交金额")
    private BigDecimal tradeMoney;
    /**
     * 总市值
     */
@ApiModelProperty(value = "总市值")
    private Long totalValue;
    /**
     * 流通市值
     */
@ApiModelProperty(value = "流通市值")
    private Long circulationValue;
    /**
     * 成交量
     */
@ApiModelProperty(value = "成交量")
    private Long turnoverNum;


    /**
     * 获取主键
     */
    public Long getId() {
        return id;
    }
    /**
     * 设置主键
     */

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取日期，2019-10-16
     */
    public String getDate() {
        return date;
    }
    /**
     * 设置日期，2019-10-16
     */

    public void setDate(String date) {
        this.date = date;
    }

    /**
     * 获取股票代码
     */
    public String getCode() {
        return code;
    }
    /**
     * 设置股票代码
     */

    public void setCode(String code) {
        this.code = code;
    }

    /**
     * 获取股票名称
     */
    public String getName() {
        return name;
    }
    /**
     * 设置股票名称
     */

    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取所属市场
     */
    public String getMarket() {
        return market;
    }
    /**
     * 设置所属市场
     */

    public void setMarket(String market) {
        this.market = market;
    }

    /**
     * 获取收盘价
     */
    public BigDecimal getClosePrice() {
        return closePrice;
    }
    /**
     * 设置收盘价
     */

    public void setClosePrice(BigDecimal closePrice) {
        this.closePrice = closePrice;
    }

    /**
     * 获取最高价
     */
    public BigDecimal getMaxPrice() {
        return maxPrice;
    }
    /**
     * 设置最高价
     */

    public void setMaxPrice(BigDecimal maxPrice) {
        this.maxPrice = maxPrice;
    }

    /**
     * 获取最低价
     */
    public BigDecimal getMinPrice() {
        return minPrice;
    }
    /**
     * 设置最低价
     */

    public void setMinPrice(BigDecimal minPrice) {
        this.minPrice = minPrice;
    }

    /**
     * 获取开盘价
     */
    public BigDecimal getOpenPrice() {
        return openPrice;
    }
    /**
     * 设置开盘价
     */

    public void setOpenPrice(BigDecimal openPrice) {
        this.openPrice = openPrice;
    }

    /**
     * 获取前收盘
     */
    public BigDecimal getBeforeClose() {
        return beforeClose;
    }
    /**
     * 设置前收盘
     */

    public void setBeforeClose(BigDecimal beforeClose) {
        this.beforeClose = beforeClose;
    }

    /**
     * 获取涨跌额
     */
    public BigDecimal getChangeAmount() {
        return changeAmount;
    }
    /**
     * 设置涨跌额
     */

    public void setChangeAmount(BigDecimal changeAmount) {
        this.changeAmount = changeAmount;
    }

    /**
     * 获取涨跌幅
     */
    public BigDecimal getChangeRange() {
        return changeRange;
    }
    /**
     * 设置涨跌幅
     */

    public void setChangeRange(BigDecimal changeRange) {
        this.changeRange = changeRange;
    }

    /**
     * 获取换手率
     */
    public BigDecimal getTurnoverRate() {
        return turnoverRate;
    }
    /**
     * 设置换手率
     */

    public void setTurnoverRate(BigDecimal turnoverRate) {
        this.turnoverRate = turnoverRate;
    }

    /**
     * 获取成交量
     */
    public Long getTradeNum() {
        return tradeNum;
    }
    /**
     * 设置成交量
     */

    public void setTradeNum(Long tradeNum) {
        this.tradeNum = tradeNum;
    }

    /**
     * 获取成交金额
     */
    public BigDecimal getTradeMoney() {
        return tradeMoney;
    }
    /**
     * 设置成交金额
     */

    public void setTradeMoney(BigDecimal tradeMoney) {
        this.tradeMoney = tradeMoney;
    }

    /**
     * 获取总市值
     */
    public Long getTotalValue() {
        return totalValue;
    }
    /**
     * 设置总市值
     */

    public void setTotalValue(Long totalValue) {
        this.totalValue = totalValue;
    }

    /**
     * 获取流通市值
     */
    public Long getCirculationValue() {
        return circulationValue;
    }
    /**
     * 设置流通市值
     */

    public void setCirculationValue(Long circulationValue) {
        this.circulationValue = circulationValue;
    }

    /**
     * 获取成交量
     */
    public Long getTurnoverNum() {
        return turnoverNum;
    }
    /**
     * 设置成交量
     */

    public void setTurnoverNum(Long turnoverNum) {
        this.turnoverNum = turnoverNum;
    }

    /**
     * 主键列的数据库字段名称
     */
    public static final String ID = "id";

    /**
     * 日期，2019-10-16列的数据库字段名称
     */
    public static final String DATE = "date";

    /**
     * 股票代码列的数据库字段名称
     */
    public static final String CODE = "code";

    /**
     * 股票名称列的数据库字段名称
     */
    public static final String NAME = "name";

    /**
     * 所属市场列的数据库字段名称
     */
    public static final String MARKET = "market";

    /**
     * 收盘价列的数据库字段名称
     */
    public static final String CLOSE_PRICE = "close_price";

    /**
     * 最高价列的数据库字段名称
     */
    public static final String MAX_PRICE = "max_price";

    /**
     * 最低价列的数据库字段名称
     */
    public static final String MIN_PRICE = "min_price";

    /**
     * 开盘价列的数据库字段名称
     */
    public static final String OPEN_PRICE = "open_price";

    /**
     * 前收盘列的数据库字段名称
     */
    public static final String BEFORE_CLOSE = "before_close";

    /**
     * 涨跌额列的数据库字段名称
     */
    public static final String CHANGE_AMOUNT = "change_amount";

    /**
     * 涨跌幅列的数据库字段名称
     */
    public static final String CHANGE_RANGE = "change_range";

    /**
     * 换手率列的数据库字段名称
     */
    public static final String TURNOVER_RATE = "turnover_rate";

    /**
     * 成交量列的数据库字段名称
     */
    public static final String TRADE_NUM = "trade_num";

    /**
     * 成交金额列的数据库字段名称
     */
    public static final String TRADE_MONEY = "trade_money";

    /**
     * 总市值列的数据库字段名称
     */
    public static final String TOTAL_VALUE = "total_value";

    /**
     * 流通市值列的数据库字段名称
     */
    public static final String CIRCULATION_VALUE = "circulation_value";

    /**
     * 成交量列的数据库字段名称
     */
    public static final String TURNOVER_NUM = "turnover_num";

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "ShareStockDayK{" +
        "id=" + id +
        ", date=" + date +
        ", code=" + code +
        ", name=" + name +
        ", market=" + market +
        ", closePrice=" + closePrice +
        ", maxPrice=" + maxPrice +
        ", minPrice=" + minPrice +
        ", openPrice=" + openPrice +
        ", beforeClose=" + beforeClose +
        ", changeAmount=" + changeAmount +
        ", changeRange=" + changeRange +
        ", turnoverRate=" + turnoverRate +
        ", tradeNum=" + tradeNum +
        ", tradeMoney=" + tradeMoney +
        ", totalValue=" + totalValue +
        ", circulationValue=" + circulationValue +
        ", turnoverNum=" + turnoverNum +
        "}";
    }
}
