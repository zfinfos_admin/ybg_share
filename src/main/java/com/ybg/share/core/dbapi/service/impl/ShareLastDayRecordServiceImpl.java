package com.ybg.share.core.dbapi.service.impl;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ybg.share.core.dbapi.entity.ShareLastDayRecord;
import com.ybg.share.core.dbapi.dao.ShareLastDayRecordMapper;
import com.ybg.share.core.dbapi.service.ShareLastDayRecordService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.Date;
//import com.alibaba.dubbo.config.annotation.Service;
/**
 * <p>
 * 股票最后一次加载日期执行时间 服务实现类
 * </p>
 *
 * @author yanyu
 * @since 2019-10-13
 */
//@Service(version = "1.0.0", interfaceClass = ShareLastDayRecordService.class)
@Service
public class ShareLastDayRecordServiceImpl extends ServiceImpl<ShareLastDayRecordMapper, ShareLastDayRecord> implements ShareLastDayRecordService {

    @Override
    public boolean hadRun(Date date, String code) {
        QueryWrapper<ShareLastDayRecord> wrapper=new QueryWrapper<>();
        wrapper.eq(ShareLastDayRecord.CODE,code);
        wrapper.eq(ShareLastDayRecord.LAST_DATE, DateUtil.formatDate(date));
        ShareLastDayRecord record= baseMapper.selectOne(wrapper);
        return record!=null;
    }
}
