package com.ybg.share.core.dbapi.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ybg.share.core.dbapi.dao.ShareStockMapper;
import com.ybg.share.core.dbapi.entity.ShareStock;
import com.ybg.share.core.dbapi.service.ShareStockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
//import com.alibaba.dubbo.config.annotation.Service;

/**
 * <p>
 * 股票信息 服务实现类
 * </p>
 *
 * @author yanyu
 * @since 2019-10-13
 */
//@Service(version = "1.0.0", interfaceClass = ShareStockService.class)
@Service
public class ShareStockServiceImpl extends ServiceImpl<ShareStockMapper, ShareStock> implements ShareStockService {
    private ShareStockMapper shareStockMapper;
    @Autowired
    public void setShareStockMapper(ShareStockMapper shareStockMapper) {
        this.shareStockMapper = shareStockMapper;
    }

    @Override
    public boolean saveIgnore(ShareStock shareStock) {
        return shareStockMapper.saveIgnore(shareStock);
    }

    @Override
    public boolean saveReplace(ShareStock shareStock) {
        return shareStockMapper.saveReplace(shareStock);
    }
}
