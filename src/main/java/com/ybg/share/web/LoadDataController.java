package com.ybg.share.web;

import com.ybg.share.core.shareapi.impl.*;
import com.ybg.share.framework.R;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoadDataController {


    @RequestMapping("/api/loadData")
    public R loadData() {

        return new R(R.SUCCESS);
    }

    @Autowired
    EastMoneyStockInfoApi eastMoneyStockInfoApi;

    @ApiOperation("拉取股票列表（深圳和上海交易所）")
    @RequestMapping("/api/loadStockData")
    public R loadStockData() {
        eastMoneyStockInfoApi.execute();
        return new R(R.SUCCESS);
    }

    @Autowired
    ShanghaiStockIndexApi shanghaiStockIndexApi;
    @Autowired
    ShenzhenStockIndexApi shenZhenStockIndexApi;

    @ApiOperation("大盘日K数据（全量）")
    @RequestMapping("/api/loadCompositeIndexData")
    public R loadCompositeIndexData(){
        shanghaiStockIndexApi.execute();
        shenZhenStockIndexApi.execute();
        return new R(R.SUCCESS).setMsg("执行完毕");
    }

    @Autowired
    ShanghaiStockDayKApi shanghaiStockDayKApi;
    @Autowired
    ShenzhenStockDayKApi shenzhenStockDayKApi;

    @ApiOperation("日K数据")
    @RequestMapping("/api/loadStockDayKData")
    public R loadStockDayKData(){
        shanghaiStockDayKApi.execute();
        shenzhenStockDayKApi.execute();
        return new R(R.SUCCESS).setMsg("执行完毕");
    }

}
