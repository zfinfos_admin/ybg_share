package com.ybg.share.schedule;

import com.ybg.share.spider.StockProcessor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import us.codecraft.webmagic.Spider;

@Component
public class StockJob {

    @Value("${url.stock_list}")
    String STOCK_LIST;

    @Scheduled(cron = "0 30 2 * * ? *")
    public void updateStockData(){
        Spider.create(new StockProcessor())
                .addUrl(STOCK_LIST)
                .thread(100).run();
    }
}
