package com.ybg.share.framework.enums;

/**
 * 股票市场
 */
public enum MarketEnum {
    /**
     * 深圳交易所
     */
    sz("sz", "深圳交易所","19901219"),
    /**
     * 上海交易所
     */
    sh("sh", "上海交易所","19901219");
    String code;
    String marketName;
    String startTime;

    /**
     * 市场标识
     * @return
     */
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    /**
     * 获取市场名称
     * @return String marketName
     */
    public String getMarketName() {
        return marketName;
    }

    public void setMarketName(String marketName) {
        this.marketName = marketName;
    }

    /**
     * 获取市场开始时间(标准时间以大盘的开始时间为准)
     * @return String date
     */
    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    MarketEnum(String code, String marketName,String startTime) {
        this.code = code;
        this.marketName = marketName;
        this.startTime = startTime;
    }
}
